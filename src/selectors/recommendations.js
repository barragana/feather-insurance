export const getRecommendationsData = ({ recommendations }) => recommendations.data;
export const isRecommendationsReady = ({ recommendations }) => !!recommendations.data.length;
export const getRecommendationsRequestInfo = ({ recommendations }) => recommendations.request;
