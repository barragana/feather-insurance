export const getUserData = ({ user }) => user.data;
export const getUserJWT = ({ user }) => user.session.getUserJWT;
export const getUserRequestInfo = ({ user }) => user.request;
