import { combineReducers } from 'redux';

import user from './user';
import recommendations from './recommendations';

const rootDucks = combineReducers({
  user,
  recommendations
});

export default rootDucks;
