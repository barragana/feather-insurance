export const FETCH_RECOMMENDATIONS_REQUESTED = 'FETCH_RECOMMENDATIONS_REQUESTED';
export const fetchRecommendationsRequested = jwt => ({ type: FETCH_RECOMMENDATIONS_REQUESTED, payload: { jwt } });

export const FETCH_RECOMMENDATIONS_SUCCESSED = 'FETCH_RECOMMENDATIONS_SUCCESSED';
export const fetchRecommendationsSuccessed = recommendations => ({ type: FETCH_RECOMMENDATIONS_SUCCESSED, payload: { recommendations } });

export const FETCH_RECOMMENDATIONS_FAILED = 'FETCH_RECOMMENDATIONS_FAILED';
export const fetchRecommendationsFailed = error => ({ type: FETCH_RECOMMENDATIONS_FAILED, payload: { error } });

export const initialState = {
  request: {
    isFetching: false,
    didInvalidate: false,
    error: null,
  },
  data: [],
};

const sortingReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_RECOMMENDATIONS_REQUESTED:
      return {
        ...state,
        request: {
          isFetching: true,
          didInvalidate: false,
          error: null,
        },
      };

    case FETCH_RECOMMENDATIONS_SUCCESSED:
      return {
        ...state,
        request: {
          ...state.request,
          isFetching: false,
        },
        data: payload.recommendations,
      };

    case FETCH_RECOMMENDATIONS_FAILED:
      return {
        ...state,
        request: {
          isFetching: false,
          didInvalidate: true,
          error: payload.error,
        },
      };

    default:
      return state;
  }
};

export default sortingReducer;
