import { getJWTFromLocalStorage, getUserFromLocalStorage } from "../services/localStorage";

export const QUESTIONNAIRE_FIELD_CHANGE = 'QUESTIONNAIRE_FIELD_CHANGE';
export const questionnaireFieldChange = (id, value) => ({ type: QUESTIONNAIRE_FIELD_CHANGE, payload: { id, value } });

export const SUBMIT_USER_REQUESTED = 'SUBMIT_USER_REQUESTED';
export const submitUserRequested = user => ({ type: SUBMIT_USER_REQUESTED, payload: { user } });

export const SUBMIT_USER_SUCCESSED = 'SUBMIT_USER_SUCCESSED';
export const submitUserSuccessed = jwt => ({ type: SUBMIT_USER_SUCCESSED, payload: { jwt } });

export const SUBMIT_USER_FAILED = 'SUBMIT_USER_FAILED';
export const submitUserFailed = error => ({ type: SUBMIT_USER_FAILED, payload: { error } });

const userInitialState = {
  firstName: '',
  address: '',
  hasChildren: null,
  numberOfChildren: '',
  occupation: '',
  email: ''
}

export const initialState = {
  request: {
    isSubmitting: false,
    didInvalidate: false,
    error: null,
  },
  data: { ...userInitialState, ...getUserFromLocalStorage() },
  session: {
    jwt: getJWTFromLocalStorage() || '',
  },
};

const stationsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case QUESTIONNAIRE_FIELD_CHANGE:
      return {
        ...state,
        data: {
          ...state.data,
          [payload.id]: payload.value,
        },
      };

    case SUBMIT_USER_REQUESTED:
      return {
        ...state,
        request: {
          isSubmitting: true,
          didInvalidate: false,
          error: null,
        }
      };

    case SUBMIT_USER_SUCCESSED:
      return {
        ...state,
        request: {
          isSubmitting: false,
          didInvalidate: false,
          error: null,
        },
        session: {
          jwt: payload.jwt,
        },
      };

    case SUBMIT_USER_FAILED:
      return {
        ...state,
        request: {
          isSubmitting: false,
          didInvalidate: true,
          error: payload.error,
        },
      };

    default:
      return state;
  }
};

export default stationsReducer;
