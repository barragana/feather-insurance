export const dataType = {
  STRING: 'text',
  NUMBER: 'number',
  RADIO: 'radio',
  EMAIL: 'email',
};

export const componentType = {
  INPUT_TEXT: 'InputText',
  INPUT_RADIO: 'InputRadio',
};

export const fieldType = {
  DATA: 'data',
  FLOW: 'flow',
};

const questionnaire = {
  firstName: {
    id: 'firstName',
    label: 'What’s your first name?',
    componentType: componentType.INPUT_TEXT,
    fieldType: fieldType.DATA,
    type: dataType.STRING,
    route: '/firstName',
    nextRoute: () => '/address',
  },
  address: {
    id: 'address',
    label: 'What’s your full address?',
    componentType: componentType.INPUT_TEXT,
    fieldType: fieldType.DATA,
    type: dataType.STRING,
    route: '/address',
    nextRoute: () => '/occupation',
  },
  occupation: {
    id: 'occupation',
    label: 'What’s your occupation?',
    componentType: componentType.INPUT_RADIO,
    options: [
      {
        label: 'Employed',
        value: 'EMPLOYED',
      },
      {
        label: 'Self employed',
        value: 'SELF_EMPLOYED',
      },
      {
        label: 'Student',
        value: 'STUDENT',
      },
    ],
    fieldType: fieldType.DATA,
    type: dataType.RADIO,
    route: '/occupation',
    nextRoute: () => '/hasChildren',
  },
  hasChildren: {
    id: 'hasChildren',
    label: 'Do you have any children?',
    componentType: componentType.INPUT_RADIO,
    options: [
      {
        label: 'Yes',
        value: true,
      },
      {
        label: 'No',
        value: false,
      },
    ],
    fieldType: fieldType.FLOW,
    route: '/3',
    type: dataType.RADIO,
    nextRoute: value => value ? '/numberOfChildren' : '/email',
  },
  numberOfChildren: {
    id: 'numberOfChildren',
    label: 'How many children do you have?',
    componentType: componentType.INPUT_TEXT,
    fieldType: fieldType.DATA,
    type: dataType.NUMBER,
    route: '/numberOfChildren',
    nextRoute: () => '/email',
    cast: user => user.hasChildren ? Number(user.numberOfChildren) : 0,
  },
  email: {
    id: 'email',
    label: 'What’s your email?',
    componentType: componentType.INPUT_TEXT,
    fieldType: fieldType.DATA,
    type: dataType.STRING,
    route: '/email',
    nextRoute: () => '',
  }
};

export default questionnaire;
