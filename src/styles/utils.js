
import { css } from 'styled-components';

export const withClickableStyle = ({ disabled, onClick }) =>
  !disabled && onClick && css`
    cursor: pointer;
  `;
