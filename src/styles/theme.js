import { createGlobalStyle } from 'styled-components';

export const colors = {
  p1: '#800060',
  p7: '#600080',

  n2: '#fafbfc',
  n3: '#bac2cb',
  n4: '#dee1e5',
  n5: '#c1c7cd',
  n6: '#89949e',
  n7: '#4e5f6e',
  n9: '#13293d',
}

export const spacing = {
  xxs: '4px',
  xs: '8px',
  s: '10px',
  m: '12px',
  l: '14px',
  xl: '18px',
  xxl: '24px',
};

export const borderRadius = {
  s: '1px',
  m: '2px',
  l: '4px',
};

export const fontSizes = {
  s: {
    'font-size': '12px',
    'line-height': '1.4',
  },
  m: {
    'font-size': '14px',
    'line-height': '1.6',
  },
  l: {
    'font-size': '16px',
    'line-height': '1.8',
  },
  xl: {
    'font-size': '24px',
    'line-height': '2.6',
  }
}

const theme = {
  colors,
  fontSizes,
  spacing,
  borderRadius,
};

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }

  * {
    box-sizing: border-box;
    color: ${({ theme }) => theme.colors.n9};
  }
`;

export default theme;
