import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import Layout from '../components/Layout';
import RecommendationsList from '../components/RecommendationsList';
import { fetchRecommendationsRequested } from '../ducks/recommendations';
import { QUESTIONNAIRE_ROUTE } from '../routes';

import { getRecommendationsData, getRecommendationsRequestInfo, isRecommendationsReady } from '../selectors/recommendations';
import { getJWTFromLocalStorage } from '../services/localStorage';

const CenteredWrapper = styled.div`
  width: 500px;
`;

const Text = styled.div`
  ${({ theme }) => theme.fontSizes.s};
`;

const Recommendations = ({ recommendations, request, requestRecommendations, isReady }) => {
  const history = useHistory();
  const [jwt] = useState(getJWTFromLocalStorage());

  useEffect(() => {
    if (!jwt) history.push(`${QUESTIONNAIRE_ROUTE}/firstName`);
    if (!isReady) requestRecommendations(jwt);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if(request.didInvalidate) {
      history.push(`${QUESTIONNAIRE_ROUTE}/firstName`);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [request.didInvalidate])

  if (!isReady || request.isFetching) return <Layout>Loading ...</Layout>;

  return (
    <Layout>
      <CenteredWrapper>
        <h1>We got your recommendation</h1>
        <Text>Based on your answers, this is what make sense for you and what you should pay.</Text>
        <RecommendationsList recommendations={recommendations} />
      </CenteredWrapper>
    </Layout>
  );
};

const mapStateToProps = state => ({
  request: getRecommendationsRequestInfo(state),
  recommendations: getRecommendationsData(state),
  isReady: isRecommendationsReady(state),
});

const mapDispatchToProps = {
  requestRecommendations: fetchRecommendationsRequested,
};

export default connect(mapStateToProps, mapDispatchToProps)(Recommendations);
