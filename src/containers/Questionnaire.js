import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";

import questionnaire from '../config/questionnaire';
import { QUESTIONNAIRE_ROUTE } from '../routes';

import Field from '../components/Field';
import Layout from '../components/Layout';

import { questionnaireFieldChange, submitUserRequested } from '../ducks/user';
import { getUserData, getUserRequestInfo } from '../selectors/user';
import { isRecommendationsReady } from '../selectors/recommendations';
import { setUserToLocalStorage } from '../services/localStorage';

const Questionnaire = ({ user, request, onSubmit, onChange, isRecommendationsReady }) => {
  const { question } = useParams();
  const history = useHistory();

  useEffect(() => {
    if (question !== 'firstName') history.push(`${QUESTIONNAIRE_ROUTE}/firstName`);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fieldConfig = questionnaire[question];
  if (!fieldConfig) return <Layout>Loading ...</Layout>;

  const fieldId = fieldConfig.id;
  const fieldValue = user[fieldId];

  const nextRoute = fieldConfig.nextRoute(fieldValue);

  const buttonLabel = (request.isSubmitting && 'Submitting...') ||
    (nextRoute && 'Next') ||
    'Submit';

  const handleClick = () => {
    setUserToLocalStorage(user);
    if (!nextRoute) return onSubmit(user);
    history.push(`${QUESTIONNAIRE_ROUTE}${nextRoute}`);
  }

  return (
    <Layout>
      <Field
        config={fieldConfig}
        value={fieldValue}
        onChange={value => {
          onChange(fieldId, value)
        }}
        onClick={handleClick}
        buttonLabel={buttonLabel}
        disabled={request.isSubmitting}
        error={request.error}
      />
    </Layout>
  );
};

const mapStateToProps = state => ({
  request: getUserRequestInfo(state),
  user: getUserData(state),
  isRecommendationsReady: isRecommendationsReady(state),
});

const mapDispatchToProps = {
  onSubmit: submitUserRequested,
  onChange: questionnaireFieldChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(Questionnaire);
