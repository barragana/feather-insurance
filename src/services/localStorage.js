export const localStorageKeys = {
  USER: 'user',
  JWT: 'jwt_token',
}

export const getUserFromLocalStorage = () =>
  JSON.parse(localStorage.getItem(localStorageKeys.USER));

export const removeUserFromLocalStorage = () =>
  localStorage.removeItem(localStorageKeys.USER);

export const setUserToLocalStorage = user =>
  localStorage.setItem(
    localStorageKeys.USER,
    JSON.stringify(user),
  );

export const setJWTToLocalStorage = jwt =>
  localStorage.setItem(localStorageKeys.JWT, jwt);

export const getJWTFromLocalStorage = () =>
  localStorage.getItem(localStorageKeys.JWT);
