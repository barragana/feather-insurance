import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Questionnaire from './containers/Questionnaire';
import Recommendations from './containers/Recommendations';
import { QUESTIONNAIRE_ROUTE, RECOMMENDATIONS_ROUTE } from './routes';

const AppRouter = () => {
  return (
    <Switch>
      <Route path={`${QUESTIONNAIRE_ROUTE}/:question`} component={Questionnaire} />
      <Route path={RECOMMENDATIONS_ROUTE} component={Recommendations} />
      <Redirect to={`${QUESTIONNAIRE_ROUTE}/firstName`} />
    </Switch>
  );
};

export default AppRouter;
