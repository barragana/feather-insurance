import styled from 'styled-components';

const Layout = styled.div`
  align-items: center;
  display: flex;
  height: 100vh;
  justify-content: center;
  margin: ${({ theme }) => theme.spacing.xl};
  width: 100vw;
`;

export default Layout;
