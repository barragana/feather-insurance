import { createRef } from 'react';


import questionnaire from '../config/questionnaire';
import Input from './Input';
import { renderWithTheme, fireEvent } from '../testUtils';

describe('Components Input', () => {
  test('given a radio component type should render InputRadio', () => {
    const config = questionnaire.occupation;
    const inputRef = createRef();
    const mockOnChange = jest.fn();

    const { container, getByText } = renderWithTheme(
      <Input
        inputRef={inputRef}
        id={config.id}
        name={config.id}
        type= {config.type}
        onChange={mockOnChange}
        value={''}
        config={config}
        disabled={false}
      />
    );


    const input = container.querySelectorAll('[type="radio"]');
    expect(input).toHaveLength(3);

    expect(getByText(config.options[0].label)).toBeTruthy();
    expect(getByText(config.options[1].label)).toBeTruthy();
    expect(getByText(config.options[2].label)).toBeTruthy();

    const option = container.querySelector(`[value="${config.options[0].value}"]`);
    fireEvent.click(option);
    expect(mockOnChange).toBeCalledTimes(1);
    expect(mockOnChange).toBeCalledWith('EMPLOYED');
  });

  test('given a text component type should render InputText', () => {
    const config = questionnaire.firstName;
    const inputRef = createRef();
    const mockOnChange = jest.fn();

    const { getByTestId } = renderWithTheme(
      <Input
        inputRef={inputRef}
        id={config.id}
        name={config.id}
        type= {config.type}
        onChange={mockOnChange}
        value={''}
        config={config}
        disabled={false}
      />
    );


    const input = getByTestId(`input-${config.id}`);
    fireEvent.change(input, { target: { value: 'FirstName' } });
    expect(mockOnChange).toBeCalledTimes(1);
    expect(mockOnChange).toBeCalledWith('FirstName');
  });
});
