import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  align-items: center;
  display: flex;
  height: 100vh;
  margin: ${({ theme }) => theme.spacing.xxl};
  width: 100vw;
`;

const Field = ({ label, config, value, onChange, onClick, buttonLabel }) => (
  <Wrapper>
    <Field
      config={config}
      label={label}
      value={value}
      onChange={onChange}
      onClick={onClick}
      buttonLabel={buttonLabel}
    />
  </Wrapper>
)

export default Field;
