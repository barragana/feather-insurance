import questionnaire from '../config/questionnaire';
import { renderWithTheme, fireEvent } from '../testUtils';
import Field from './Field';

describe('Components Field', () => {
  test('given a config should render all elements', () => {
    const config = questionnaire.firstName;
    const mockOnChange = jest.fn();
    const mockOnClick = jest.fn();

    const { getByText, getByTestId } = renderWithTheme(
      <Field
        config={config}
        value={''}
        onChange={mockOnChange}
        onClick={mockOnClick}
        buttonLabel={'Button Label'}
        disabled={false}
        error=""
      />
    );

    expect(getByText(config.label)).toBeTruthy();
    expect(getByTestId(`input-${config.id}`)).toBeTruthy();
    expect(getByText('Button Label')).toBeTruthy();
  });

  test('given a input change it should be trigger on Field', () => {
    const config = questionnaire.firstName;
    const mockOnChange = jest.fn();
    const mockOnClick = jest.fn();

    const { getByTestId } = renderWithTheme(
      <Field
        config={config}
        value={''}
        onChange={mockOnChange}
        onClick={mockOnClick}
        buttonLabel={'Button Label'}
        disabled={false}
        error=""
      />
    );

    const input = getByTestId(`input-${config.id}`);
    fireEvent.change(input, { target: { value: 'FirstName' } });
    expect(mockOnChange).toBeCalledWith('FirstName');
  });

  test('given a input value empty it should disable button', () => {
    const config = questionnaire.firstName;
    const mockOnChange = jest.fn();
    const mockOnClick = jest.fn();

    const { getByText } = renderWithTheme(
      <Field
        config={config}
        value={''}
        onChange={mockOnChange}
        onClick={mockOnClick}
        buttonLabel={'Button Label'}
        disabled={false}
        error=""
      />
    );

    const button = getByText('Button Label');
    fireEvent.click(button);
    expect(mockOnClick).not.toBeCalled();
  });

  test('given a disabled prop is true it should disable button', () => {
    const config = questionnaire.firstName;
    const mockOnChange = jest.fn();
    const mockOnClick = jest.fn();

    const { getByText } = renderWithTheme(
      <Field
        config={config}
        value={'Some value'}
        onChange={mockOnChange}
        onClick={mockOnClick}
        buttonLabel={'Button Label'}
        disabled={true}
        error=""
      />
    );

    const button = getByText('Button Label');
    fireEvent.click(button);
    expect(mockOnClick).not.toBeCalled();
  });
});
