import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  align-items: center;
  border: 1px solid ${({ theme }) => theme.colors.n5};
  border-radius: ${({ theme }) => theme.borderRadius.l};
  display: flex;
  height: 50px;
  justify-content: space-between;
  margin: ${({ theme }) => theme.spacing.m} 0;
  padding: ${({ theme }) => `${theme.spacing.l} ${theme.spacing.m}`};
  ${({ theme }) => theme.fontSizes.m};
  width: 100%;
`;

const Price = styled.div`
  ${({ theme }) => theme.fontSizes.m};
`;

export const recommendationType = type => {
  const lowercase = type.replace(/_/g, ' ').toLowerCase();
  return `${lowercase[0].toUpperCase()}${lowercase.substr(1, lowercase.length)}`;
}

const Recommendation = ({ type, price }) => (
  <Wrapper>
    <div>{recommendationType(type)}</div>
    <Price>{price.toLowerCase()}</Price>
  </Wrapper>
);

const WrapperList = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;



const RecommendationsList = ({ recommendations }) => (
  <WrapperList>
    {recommendations.map(({ type, price }) => (
      <Recommendation key={type} type={type} price={`€${price.amount} per ${price.periodicity}`} />
    ))}
  </WrapperList>
)

export default RecommendationsList;
