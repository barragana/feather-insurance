import React, { memo } from 'react';

import InputRadio from './InputRadio';
import InputText from './InputText';

import { componentType } from '../config/questionnaire';

const Input = memo(({ config, inputRef, ...props }) => {
  if (config.componentType === componentType.INPUT_RADIO) {
    return <InputRadio options={config.options} {...props} />;
  }
  return <InputText ref={inputRef} {...props} />;
}, (prev, next) => (
  prev.config.componentType === next.config.componentType &&
  prev.value === next.value
));

export default Input;
