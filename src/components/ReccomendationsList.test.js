import { recommendationType } from "./RecommendationsList";
describe('Components RecommendationsList', () => {
  test('recommendationType should return text with uppercase first letter', () => {
    const type = 'HEALTH_INSURANCE_SOMETHING_ELSE';
    expect(recommendationType(type)).toEqual('Health insurance something else');
  });
})
