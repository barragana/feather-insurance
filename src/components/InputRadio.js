import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: grid;
  row-gap: ${({ theme }) => theme.spacing.m};
`;

const InputRadio = ({id, options, onChange, value }) => (
  <Wrapper>
    {
      options.map(({ label, value: optionValue }, index) => (
        <div key={optionValue}>
          <input
            type="radio"
            name={id}
            value={optionValue}
            onChange={e => onChange(optionValue)}
            checked={`${value}` === `${optionValue}`}
          />
          <label onClick={() => onChange(optionValue)}>
            {label}
          </label>
        </div>
      ))
    }
  </Wrapper>
);

export default InputRadio;
