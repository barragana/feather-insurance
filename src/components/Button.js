import styled from 'styled-components';
import { withClickableStyle } from '../styles/utils';

const Button = styled.button`
  background-color: ${({ theme }) => theme.colors.p7};
  border: 1px solid ${({ theme }) => theme.colors.n5};
  border-radius: ${({ theme }) => theme.borderRadius.l};
  color: white;
  width: 100%;
  ${({ theme }) => theme.fontSizes.l};
  ${withClickableStyle};

  :disabled {
    background-color: ${({ theme }) => theme.colors.n5};
  }
`;

export default Button;
