import styled from 'styled-components';

const Error = styled.div`
  color: red;
  ${({ theme }) => theme.fontSizes.l};
`;

export default Error;
