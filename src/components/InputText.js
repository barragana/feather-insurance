import React, { forwardRef } from 'react';
import styled from 'styled-components';

const StyledInputText = styled.input`
  border: 1px solid ${({ theme }) => theme.colors.n5};
  border-radius: ${({ theme }) => theme.borderRadius.l};
  width: 100%;
  ${({ theme }) => theme.fontSizes.l};
`;

const InputText = forwardRef(({ onChange, ...props}, ref) => (
  <StyledInputText
    data-testid={`input-${props.id}`}
    ref={ref}
    onChange={e => onChange(e.target.value)}
    {...props}
  />
));

InputText.defaultProps = {
  type: 'text',
};

export default InputText;
