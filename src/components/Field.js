import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';

import Input from './Input';
import Button from './Button';
import Error from './Error';

const Label = styled.div`
  font-weight: bold;
  ${({ theme }) => theme.fontSizes.xl};
`;

const Wrapper = styled.div`
  display: grid;
  grid-template-rows: repeat(3, auto);
  row-gap: ${({ theme }) => theme.spacing.m};
  width: 500px;
`;

const Field = ({ config, error, value, onChange, onClick, buttonLabel, disabled }) => {
  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef.current) inputRef.current.focus();
  }, [config.id]);

  return (
    <Wrapper>
      <Label>{config.label}</Label>
      <Input
        inputRef={inputRef}
        id={config.id}
        name={config.id}
        type= {config.type}
        onChange={onChange}
        value={value}
        config={config}
        disabled={disabled}
      />
      {error && <>{Object.keys(error).map(key =>(
        <Error key={key}>{key}: {error[key]}</Error>
      ))}</>}
      <Button
        disabled={disabled || (!value && value !== false)}
        onClick={onClick}
      >
        {buttonLabel}
      </Button>
    </Wrapper>
  );
};

export default Field;
