import React from 'react';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { Route, Router } from 'react-router-dom';

import theme, { GlobalStyle } from './styles/theme';
import browserHistory from './browserHistory';
import AppRouter from './AppRouter';
import store from './store';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route path="/" component={AppRouter} />
        </Router>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
