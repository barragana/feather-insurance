import request from "./request";

export function postQuestionnaire(data) {
  return request('/user', {
    method: 'POST',
    body: JSON.stringify(data)
  });
}

export function getRecommendations(jwt) {
  return request('/recommendation', {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  })
}
