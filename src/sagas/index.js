import { all, fork } from 'redux-saga/effects';

import user from './user';
import recommendations from './recommendations';
import integrations from './integrations';

export default function* rootSaga() {
  yield all([
    fork(user),
    fork(recommendations),
    fork(integrations),
  ]);
}
