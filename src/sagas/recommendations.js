import { all, takeLatest, put, call } from 'redux-saga/effects';
import { getRecommendations } from '../api/user';
import { fetchRecommendationsFailed, fetchRecommendationsSuccessed, FETCH_RECOMMENDATIONS_REQUESTED } from '../ducks/recommendations';

export function* handleRecommendations({ payload: { jwt }}) {
  try {
    const recommendations = yield call(getRecommendations, jwt);
    yield put(fetchRecommendationsSuccessed(recommendations));
  } catch (e) {
    yield put(fetchRecommendationsFailed(e));
  }
};

export default function* watchData() {
  yield all([takeLatest(FETCH_RECOMMENDATIONS_REQUESTED, handleRecommendations)]);
}
