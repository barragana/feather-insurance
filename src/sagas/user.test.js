import { createBody } from "./user";

describe('Saga User', () => {
  test('createBody should return payload attributes casted to appropriate data type', () => {
    const user = {
      firstName: 'FirstName',
      address: 'Address',
      hasChildren: true,
      numberOfChildren: '2',
      occupation: 'EMPLOYEE',
      email: 'test@test.com'
    }

    expect(createBody(user)).toEqual({
      firstName: 'FirstName',
      address: 'Address',
      numberOfChildren: 2,
      occupation: 'EMPLOYEE',
      email: 'test@test.com'
    });

    user.hasChildren = false;

    expect(createBody(user)).toEqual({
      firstName: 'FirstName',
      address: 'Address',
      numberOfChildren: 0,
      occupation: 'EMPLOYEE',
      email: 'test@test.com'
    });
  });
})
