import { all, takeLatest, put, call } from 'redux-saga/effects';

import { postQuestionnaire } from '../api/user';
import questionnaire, { fieldType } from '../config/questionnaire';
import {
  submitUserFailed,
  submitUserSuccessed,
  SUBMIT_USER_REQUESTED,
} from '../ducks/user';
import { setJWTToLocalStorage } from '../services/localStorage';

export const createBody = (user) => {
  return Object.keys(user).reduce((payload, key) => {
    if(questionnaire[key].fieldType === fieldType.DATA) {
      payload[key] = questionnaire[key].cast?.(user) ?? user[key];
    }
    return payload;
  }, {});
}

export function* handleSubmit({ payload: { user }}) {
  try {
    const body = yield call(createBody, user);
    const { jwt } = yield call(postQuestionnaire, body);
    yield call(setJWTToLocalStorage, jwt);
    yield put(submitUserSuccessed(jwt));
  } catch (e) {
    yield put(submitUserFailed(e.errors));
  }
};

export default function* watchData() {
  yield all([
    takeLatest(SUBMIT_USER_REQUESTED, handleSubmit),
  ]);
}
