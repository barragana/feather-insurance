import { all, takeLatest, put, call } from 'redux-saga/effects';

import browserHistory from '../browserHistory';
import { fetchRecommendationsRequested } from '../ducks/recommendations';
import { SUBMIT_USER_SUCCESSED } from '../ducks/user';
import { RECOMMENDATIONS_ROUTE } from '../routes';
import { removeUserFromLocalStorage } from '../services/localStorage';

export const pushHistory = (path) => {
  browserHistory.push(path);
}

export function* fetchRecommendationsWhenSubmitUser({ payload: { jwt }}) {
  yield put(fetchRecommendationsRequested(jwt));
  yield call(pushHistory, RECOMMENDATIONS_ROUTE);
  yield call(removeUserFromLocalStorage);
};

export default function* watchData() {
  yield all([takeLatest(SUBMIT_USER_SUCCESSED, fetchRecommendationsWhenSubmitUser)]);
}
